package fuzzy

type Results map[string]map[string]Result

func (r Results) Best(variable string) Result {
	var best Result

	for _, res := range r[variable] {
		func(res Result) {
			if res.TruthDegree() > best.TruthDegree() {
				best = res
			}
		}(res)
	}

	return best
}

type Result struct {
	term        string
	truthDegree float64
	membership  Membership
}

func (r *Result) Term() string {
	return r.term
}

func (r *Result) TruthDegree() float64 {
	return r.truthDegree
}

func (r *Result) Membership() Membership {
	return r.membership
}

func (r *Result) Value() float64 {
	return xCentroid(r.membership)
}

func xCentroid(m Membership) float64 {
	var (
		num float64
		den float64
	)

	min, max := m.Domain()

	for i := min; i <= max; i++ {
		num += m.Value(i) * i
		den += m.Value(i)
	}

	return num / den
}

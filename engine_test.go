package fuzzy

import (
	"log"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
)

func TestEngine(t *testing.T) {
	engine := NewEngine()

	engine.Variables(
		NewVariable(
			"temperature",
			// La température est "cold" (froide) en dessous de 10°
			// On considère "cold" particulièrement vrai
			// si la température descend en dessous de -10°
			NewTerm("cold", Inverted(Linear(-10, 5))),

			// La température est "cool" (fraiche) entre 0° et 20°
			// On considère "cool" particulièrement vrai
			// lorsque la temperature == 10°
			NewTerm("cool", Triangular(5, 10, 20)),

			// La température est "ok" (acceptable) entre 15° et 25°
			// On considère "ok" particulièrement vrai
			// lorsque la temperature == 20°
			NewTerm("ok", Triangular(18, 20, 30)),

			// La température est "warm" (chaude) entre 15° et 30°
			// On considère "warm" particulièrement vrai
			// lorsque la temperature == 25°
			NewTerm("warm", Triangular(15, 25, 50)),

			// La température est "hot" (étouffante) à partir de 25°
			// On considère "hot" particulièrement vrai
			// si la température dépasse 30°
			NewTerm("hot", Linear(15, 30)),
		),
		NewVariable(
			"air-conditioning",
			// Soit une climatisation avec 3 états: "cooling", "stopped", "heating"
			// La puissance du "cooling" évolue de 0 (puissance nulle) à 100 (puissance maximum)
			// "stopped" n'est pas variable: il est actif ou pas
			// La puissance du "heating" évolue de 0 (puissance nulle) à 100 (puissance maximum)
			NewTerm("cooling", Linear(0, 100)),
			NewTerm("stopped", Constant(100)),
			NewTerm("heating", Linear(0, 100)),
		),
	)

	engine.Rules(
		If(
			Or(
				Is("temperature", "cold"),
				Is("temperature", "cool"),
			),
		).Then("air-conditioning", "heating"),
		If(
			Or(
				Is("temperature", "ok"),
				Is("temperature", "warm"),
			),
		).Then("air-conditioning", "stopped"),
		If(
			Is("temperature", "hot"),
		).Then("air-conditioning", "cooling"),
	)

	inputs := Values{
		"temperature": 30,
	}

	results, err := engine.Infer(inputs)
	if err != nil {
		t.Error(errors.WithStack(err))
	}

	spew.Dump(inputs)
	spew.Dump(results.Best("air-conditioning"))
	dumpResult(results, "air-conditioning")

}

func dumpResult(results Results, variable string) {
	log.Printf("%s", variable)
	log.Println("|")
	for term, res := range results[variable] {
		log.Printf("|--> %s", term)
		log.Println("|    |")
		log.Printf("|    |--> TruthDegree: %f", res.TruthDegree())
		log.Printf("|    |--> Value: %f", res.Value())
		log.Println("|")
	}
}

package fuzzy

import "github.com/pkg/errors"

type Values map[string]float64

type Engine struct {
	rules     []*Rule
	variables []*Variable
}

func (e *Engine) Infer(values Values) (Results, error) {
	ctx := NewContext(e.variables, values)

	outputs := make([]string, 0)

	for _, r := range e.rules {
		outputVariableName := r.conclusion.Variable()
		outputTermName := r.conclusion.Term()

		outputVariable, err := ctx.Variable(outputVariableName)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		outputTerm, err := outputVariable.Term(outputTermName)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		truthDegree, err := r.premise.Value(ctx)
		if err != nil {
			return nil, errors.WithStack(err)
		}

		ctx.AddResult(outputVariableName, outputTerm, truthDegree)

		outputs = append(outputs, outputVariableName)
	}

	return ctx.Results(), nil
}

func (e *Engine) Variables(variables ...*Variable) *Engine {
	e.variables = variables
	return e
}

func (e *Engine) Rules(rules ...*Rule) *Engine {
	e.rules = rules
	return e
}

func NewEngine() *Engine {
	return &Engine{}
}
